using System;
namespace mirror
{
	// We've matched this so that its Just Like In The Combo Box.
	public enum DecompileLanguageType
	{
		CS4,
		CS3,
		CS2,
		CS,
		IL
	}
}

