using System;
using Mono.Cecil;
using Cecil.Decompiler;
using Cecil.Decompiler.Languages;
using System.Text;
using System.IO;


namespace mirror
{
	public class Decompilation
	{
		
		public static string decompile_method(Mono.Cecil.MethodDefinition method, DecompileLanguageType type)
		{
			
			if(method.IsAbstract) return "Abstract Method";
			//else if(method.IsVirtual) return "Virtual Method";
			
			
			// this decompiles IL.
			if(type == DecompileLanguageType.IL)
			{
				StringWriter content_writer = new StringWriter();
				try {
					Cecil.Decompiler.Cil.Formatter.WriteMethodBody(content_writer, method);
				}
				catch(Exception x )
				{
					content_writer.WriteLine("!!! Something BAD happened");
					content_writer.WriteLine(x.Message);
				}
				return content_writer.ToString();

			}			
			else
			{
			
				StringWriter cont_writer = new StringWriter();
				
				ILanguage cs_lang;
				
				switch (type) {
				case DecompileLanguageType.CS4:
						cs_lang = new CSharpV4();
					break;
				case DecompileLanguageType.CS3:
					cs_lang = new CSharpV3();
					break;
				case DecompileLanguageType.CS2:
					cs_lang = new CSharpV2();
					break;
				case DecompileLanguageType.CS:
				default:
					cs_lang = new CSharp();
					break;
				}
				
				
				//CSharpV4 cs_lang = new CSharpV4();
				PlainTextFormatter fmt = new PlainTextFormatter(cont_writer);
			
				CSharpWriter w = new CSharpWriter(cs_lang, fmt);
				try {
				w.Write(method);
				}
				catch{
					cont_writer.WriteLine("Decompilation Failed");
				}
				return cont_writer.ToString();
			}
			
		}
		

		public static string decompile_class (Mono.Cecil.TypeDefinition class_def, DecompileLanguageType tx)
		{
			
			StringWriter content_writer = new StringWriter();
			
			
			foreach(CustomAttribute attr in class_def.CustomAttributes)
			{
				
			}
			
			
			
			return content_writer.ToString();
		}
				                         
				                      
		private static string dumpHex(byte[] hex)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append("{ ");
			for(int i=0;i<hex.Length;i++)
			{
				sb.AppendFormat("{0:X2}{1}", hex[i], (i==hex.Length-1?" }":", "));
			}
			
			return sb.ToString();
		}
		
	}
}

