using System;
namespace mirror
{
	public class DecompileLanguageOption
	{
		public string Name {get;set;}
		public DecompileLanguageType LangType {get;set;}
		
		public DecompileLanguageOption (string LangName, DecompileLanguageType Lang_Type )
		{
			this.Name = LangName;
			this.LangType = Lang_Type;
		}
		
		public override string ToString ()
		{
			return Name;
		}
		
		
	}
}

