using System;
using System.Collections.Generic;
using Gtk;
using GtkSharp;

//using System.Reflection;
using Mono.Cecil;


public partial class MainWindow : Gtk.Window
{

	// Holds the tree of assemblies, modules, namespaces and friends.
	TreeStore assembly_model;

	//ListStore lang_store = new ListStore(typeof(mirror.DecompileLanguageOption));

	Mono.Cecil.MethodDefinition c_method;

	GtkSourceView.SourceView sourceView = new GtkSourceView.SourceView ();

	public MainWindow () : base(Gtk.WindowType.Toplevel)
	{
		Build ();
		
		// Just because
		decompile_view.Hide ();
		
		
		lang_selector.AppendText ("C# 4");
		lang_selector.AppendText ("C# 3");
		lang_selector.AppendText ("C# 2");
		lang_selector.AppendText ("C# no transformations");
		lang_selector.AppendText ("IL Assembly");
		lang_selector.Active = 0;
		
		
		
		var AssembliesToLoad = get_assemblies ();
		
		assembly_model = new TreeStore (typeof(Gdk.Pixbuf), typeof(string), typeof(object));
		
		
		// set up the view column. there's only one, so <3
		
		TreeViewColumn c = new TreeViewColumn ();
		c.Title = "Name";
		
		// icon renderer
		CellRendererPixbuf _px_rend = new CellRendererPixbuf ();
		c.PackStart (_px_rend, false);
		c.SetAttributes (_px_rend, "pixbuf", 0);
		
		// Text renderer
		CellRendererText _lb_rend = new CellRendererText ();
		c.PackEnd (_lb_rend, true);
		c.SetAttributes (_lb_rend, "text", 1);
		
		class_view.AppendColumn (c);
		
		
		
		CrankAssemblies (AssembliesToLoad);
		
		class_view.Model = assembly_model;
		cv_window.Add (sourceView);
		sourceView.Show ();
		
		
		sourceView.Buffer = new GtkSourceView.SourceBuffer (GtkSourceView.SourceLanguageManager.Default.GetLanguage ("c-sharp"));
		
		
		sourceView.ModifyFont (Pango.FontDescription.FromString (fontbutton1.FontName));
		
		sourceView.Buffer.Text = "SUP, MANG?";
		
		sourceView.WrapMode = WrapMode.Word;
		sourceView.ShowLineNumbers = true;
		
		
		
	}


	#region Crank Assemblies

	/// <summary>
	/// TODO: write a comment.
	/// </summary>
	/// <param name="AssembliesToLoad"> A List<string> </param>
	private void CrankAssemblies (List<string> AssembliesToLoad)
	{
		
		foreach (string asm in AssembliesToLoad) {
			
			CrankSingleAssembly (asm, false);
		}
	}


	private void CrankSingleAssembly (string asm, bool isFile)
	{
		TreeIter t_iter = TreeIter.Zero;
		
		//Assembly c_asm = null;
		
		Mono.Cecil.AssemblyDefinition c_asm = null;
		
		try {
			
			//System.Reflection.Assembly t_asm = (System.Reflection.Assembly.Load (asm));
			//string AssemblyLocation = t_asm.Location;
			
			//if(System.IO.File.Exists(asm)) // Literal Path
			//	c_asm = Mono.Cecil.AssemblyFactory.GetAssembly( System.IO.File.OpenRead( asm ) );
			//else
			
			if (isFile) {
				c_asm = Mono.Cecil.AssemblyFactory.GetAssembly (asm);
			} else
				c_asm = Mono.Cecil.AssemblyFactory.GetAssembly (System.Reflection.Assembly.Load (asm).Location);
			
			t_iter = assembly_model.AppendValues (get_pixbuf ("assembly-file"), c_asm.Name.ToString ());
			
			
			
			//c_asm = Assembly.Load(asm);
			
			
		} catch (Exception exp) {
			c_asm = null;
			
			TreeIter err_iter = assembly_model.AppendValues (get_pixbuf ("emblem-noread"), "Invalid Assembly: " + asm);
			
			assembly_model.AppendValues (err_iter, get_pixbuf ("dialog-information"), exp.Message);
			assembly_model.AppendValues (err_iter, get_pixbuf ("gtk-list"), exp.StackTrace);
			return;
			
		} finally {
			if (!t_iter.Equals (TreeIter.Zero)) {
				
				
				//assembly_model.AppendValues(t_iter, get_pixbuf("application-certificate"), "SNK: "+c_asm.SecurityDeclarations);
				
				foreach (Mono.Cecil.SecurityDeclaration decl in c_asm.SecurityDeclarations) {
					
					assembly_model.AppendValues(t_iter, get_pixbuf("security-high"), decl.PermissionSet.ToXml().ToString(), decl);
					
				}
				
				var c_r_asm = Mono.Cecil.AssemblyFactory.CreateReflectionAssembly(c_asm);
				//foreach(var ev in c_r_asm.Evidence)
				//{
				//	assembly_model.AppendValues(t_iter, get_pixbuf("security-low"), ev.GetType().ToString(), ev);
				//}
				
				foreach(var ev in c_r_asm.Evidence)
				{
					if(ev is System.Security.Policy.StrongName)
						assembly_model.AppendValues(t_iter, get_pixbuf("application-certificate"), "SNK: "+(ev as System.Security.Policy.StrongName).PublicKey.ToString(), ev);
				}
				
				
				//TreeIter asm_iter = assembly_model.AppendValues("Assembly: "+c_asm.Name, c_asm);
				
				for (int c_asm_mod_iter = 0; c_asm_mod_iter < c_asm.Modules.Count; c_asm_mod_iter++) {
					
					// work through each module in the Cecil assembly. 
					var c_module = c_asm.Modules[c_asm_mod_iter];
					
					
					
					
					TreeIter m_iter = assembly_model.AppendValues (t_iter, get_pixbuf ("assembly-reference"), c_module.Name, c_module);
					
					
					#region Module references
					
					// only mscorlib and things that dont reference mscorlib should be in this category, but we'll work with it.
					if (c_module.AssemblyReferences.Count > 0) {
						// Add the References iterator.
						TreeIter reference_iter = assembly_model.AppendValues (m_iter, get_pixbuf ("references"), "References", null);
						
						// Work through each of those references and find the iterator.
						foreach (Mono.Cecil.AssemblyNameReference c_ref in c_module.AssemblyReferences) {
							assembly_model.AppendValues (reference_iter, get_pixbuf ("assembly-reference"), c_ref.Name, c_ref);
						}
						
					}
					
					#endregion
					
					#region Module Types.
					
					foreach (Mono.Cecil.TypeDefinition c_type in c_module.Types) {
						
						// FIXME: There is apparently a way to hide members in an assembly: via a NULL namespace.
						// I'm going to show them, but under the namespace "<NULL>"
						
						if (c_type.IsSpecialName || c_type.IsRuntimeSpecialName)
							continue;
						// Dont bother with special names.
						string f_namespace = (c_type.Namespace == "" ? "<NULL>" : c_type.Namespace);
						
						// attempt to find the iterator for the namespace this TypeDefinition is in.
						TreeIter _ns_iter;
						if (!FindTreeIterByName (m_iter, f_namespace, out _ns_iter)) {
							_ns_iter = assembly_model.AppendValues (m_iter, get_pixbuf ("namespace"), f_namespace, c_type.Namespace);
						}
						
						
						// Some extra stuff...
						
						string c_type_str = "class";
						if (!c_type.IsClass)
							c_type_str = "struct"; else if (c_type.IsEnum)
							c_type_str = "enum"; else if (c_type.IsInterface)
							c_type_str = "interface";
						
						
						// This is our TreeIter for the actual class. 
						TreeIter _t_iter = assembly_model.AppendValues (_ns_iter, get_pixbuf ("type-" + c_type_str), c_type.Name, c_type);
						
						foreach (Mono.Cecil.MethodDefinition constr_def in c_type.Constructors) {
							// handle constructor.
							assembly_model.AppendValues (_t_iter, get_pixbuf ("class-constructor"), constr_def.ToString (), constr_def);
						}
						
						// Now, work through the methods.
						foreach (Mono.Cecil.MethodDefinition meth_def in c_type.Methods) {
							// we should hide methods that aren't public or that are special.
							if (meth_def.IsSpecialName | meth_def.IsRuntimeSpecialName | !meth_def.IsPublic)
								continue;
							assembly_model.AppendValues (_t_iter, get_pixbuf ("class-method"), meth_def.ToString (), meth_def);
						}
						
						foreach (Mono.Cecil.EventDefinition ev_def in c_type.Events) {
							// hide special name and RuntimeSpecialName events.
							if (ev_def.IsSpecialName | ev_def.IsRuntimeSpecialName)
								continue;
							assembly_model.AppendValues (_t_iter, get_pixbuf ("class-event"), ev_def.Name, ev_def);
						}
						foreach (Mono.Cecil.PropertyDefinition prop_def in c_type.Properties) {
							if (prop_def.IsSpecialName | prop_def.IsRuntimeSpecialName)
								continue;
							assembly_model.AppendValues (_t_iter, get_pixbuf ("class-property"), prop_def.ToString (), prop_def);
						}
						
						foreach (Mono.Cecil.FieldDefinition field_def in c_type.Fields) {
							if (field_def.IsRuntimeSpecialName | field_def.IsSpecialName | field_def.IsPrivate)
								continue;
							assembly_model.AppendValues (_t_iter, get_pixbuf ("class-field"), field_def.Name, field_def);
						}
					}
					
					#endregion
					
					
				}
			}
		}
	}

	#endregion


	#region FindTreeIterByName

	/// <summary>
	/// Finds the child TreeIter with the field (1) = ChildNameToFind. This does not search with depth.  
	/// </summary>
	/// <param name="parent">
	/// A <see cref="TreeIter"/> specifying the "root" of the search.
	/// </param>
	/// <param name="ChildNameToFind">
	/// A <see cref="System.String"/>
	/// </param>
	/// <param name="theChild">
	/// A <see cref="TreeIter"/>
	/// </param>
	/// <returns>
	/// A <see cref="System.Boolean"/>
	/// </returns>
	public bool FindTreeIterByName (TreeIter parent, string ChildNameToFind, out TreeIter theChild)
	{
		
		// Current iterator child. 
		TreeIter _c_child;
		
		// If we dont actually have any children....
		if (!assembly_model.IterChildren (out _c_child, parent)) {
			theChild = parent;
			return false;
		}
		
		// otherwise, work through the value of the current child and see if there's a match.
		do {
			/* asm_model->values->(child,1) is? target */			
			if ((string)(assembly_model.GetValue (_c_child, 1)) == (ChildNameToFind)) {
				theChild = _c_child;
				return true;
			}
		} while (assembly_model.IterNext (ref _c_child));
		
		
		// if, at this point, we have not yet found a match, we aren't going to.
		theChild = parent;
		return false;
		
	}

	#endregion

	/// <summary>
	/// Gets the assemblies to load.
	/// </summary>
	/// <returns>
	/// A <see cref="List<String>"/>
	/// </returns>
	public List<String> get_assemblies ()
	{
		// get the list of assemblies we should use.
		
		List<string> Assemblies_ret = new List<string> ();
		
		Dialog d = new Dialog ("Select assemblies to load", this, DialogFlags.Modal | DialogFlags.DestroyWithParent);
		
		Label l = new Label ("Select the profiles to load");
		d.VBox.Add (l);
		l.Show ();
		
		CheckButton mono_button = new CheckButton ("Standard Mono libraries");
		CheckButton asp_button = new CheckButton ("ASP.Net libraries");
		CheckButton gtk_button = new CheckButton ("GTK# Libraries");
		
		d.VBox.Add (mono_button);
		mono_button.Show ();
		
		d.VBox.Add (asp_button);
		asp_button.Show ();
		
		d.VBox.Add (gtk_button);
		gtk_button.Show ();
		
		d.AddButton (Stock.Ok, ResponseType.Ok);
		d.Response += delegate(object o, ResponseArgs args) {
			
			if (mono_button.State == StateType.Active) {
				
				Assemblies_ret.Add ("System");
				Assemblies_ret.Add ("mscorlib.dll");
				Assemblies_ret.Add ("Mono.Posix");
				
			}
			if (gtk_button.State == StateType.Active) {
				Assemblies_ret.Add ("gtk-sharp");
				Assemblies_ret.Add ("atk-sharp");
				Assemblies_ret.Add ("gdk-sharp");
				Assemblies_ret.Add ("glib-sharp");
				Assemblies_ret.Add ("pango-sharp");
				Assemblies_ret.Add ("glade-sharp");
				Assemblies_ret.Add ("stetic");
				
			}
			if (asp_button.State == StateType.Active) {
				Assemblies_ret.Add ("System.Web.dll");
			}
			
			d.Hide ();
			d.Dispose ();
			
		};
		d.Run ();
		
		
		
		return Assemblies_ret;
	}

	protected void OnDeleteEvent (object sender, DeleteEventArgs a)
	{
		Application.Quit ();
		a.RetVal = true;
	}
	protected virtual void OnClassViewRowActivated (object o, Gtk.RowActivatedArgs args)
	{
		// ...
	}


	public void mangle_path (TreePath t_path)
	{
		TreeIter x;
		if (assembly_model.GetIter (out x, t_path)) {
			
			object current_sel = (object)assembly_model.GetValue (x, 2);
			
			// handle type.
			if (current_sel != null) {
				
				// Lets actually use this.
				
				System.IO.StringWriter sw = new System.IO.StringWriter ();
				
				//sw.WriteLine("Selected: <small><tt>{0}</tt></small>",current_sel.ToString());
				
				if (current_sel is Mono.Cecil.TypeDefinition) {
					
					
					Mono.Cecil.TypeDefinition cDef = current_sel as Mono.Cecil.TypeDefinition;
					
					//sourceView.Buffer.Text = current_sel.ToString();
					
					sw.WriteLine ("Selected Type: <tt>{0}</tt>\nMember of: <tt>{1}</tt>", cDef.Name, cDef.Namespace);
					
					sw.Write ("Declaration: <tt>");
					if (cDef.IsPublic)
						sw.Write ("public ");
					if (cDef.IsSealed)
						sw.Write ("sealed ");
					if (cDef.IsUnicodeClass)
						sw.Write ("unicode ");
					if (cDef.IsAbstract)
						sw.Write ("abstract ");
					if (cDef.IsBeforeFieldInit)
						sw.Write ("beforefieldinit ");
					
					if (cDef.IsAnsiClass)
						sw.Write ("ansi ");
					//if(cDef.IsClass) sw.Write("class");
					if (cDef.IsEnum)
						sw.Write ("enumeration"); else if (cDef.IsInterface)
						sw.Write ("interface");
					else
						sw.Write ("class");
					
					sw.WriteLine ("</tt>");
					
					// now, we decompile the current definition.
					
					System.IO.StringWriter cl_w = new System.IO.StringWriter ();
					
					
					if (cDef.HasCustomAttributes) {
						//...
						foreach (Mono.Cecil.CustomAttribute item in cDef.CustomAttributes) {
							IAnnotationProvider prov = (IAnnotationProvider)item;
							foreach (var nnon in prov.Annotations.Keys) {
								cl_w.WriteLine (nnon.ToString () + " = " + prov.Annotations[nnon].ToString ());
							}
						}
					}
					
					
					sourceView.Buffer = new GtkSourceView.SourceBuffer (GtkSourceView.SourceLanguageManager.Default.GetLanguage ("c-sharp"));
					sourceView.Buffer.Text = mirror.Decompilation.decompile_class (cDef, mirror.DecompileLanguageType.CS);
					
					
					
					decompile_view.Show ();
					
					
				} else if (current_sel is Mono.Cecil.MethodDefinition) {
					// method.
					
					c_method = (current_sel as Mono.Cecil.MethodDefinition);
					
					sw.WriteLine ("Method <tt>{0}</tt>", c_method.ToString ());
					sw.WriteLine ("Member of <tt>{0}</tt>", c_method.DeclaringType.Name);
					
					
					decompile_current ();
					
					
					
				}
				info_label.Text = sw.ToString ();
				info_label.UseMarkup = true;
				
				
				
				
				
			}
			
			
			/*Type sel_type = (Type)assembly_model.GetValue(x,1);
			if(sel_type != null)
			{
				
				sourceView.Buffer.Text = mirror.ClassDumper.get_methods(sel_type);
				
			}*/			
			
			
			
		}		
		
	}

	public void decompile_current ()
	{
		
		if (c_method != null) {
			mirror.DecompileLanguageType c_type = mirror.DecompileLanguageType.CS4;
			
			c_type = (mirror.DecompileLanguageType)(lang_selector.Active);
			
			this.Title = c_method.ToString ();
			
			decompile_view.Show ();
			
			GtkSourceView.SourceLanguage c_lang = GtkSourceView.SourceLanguageManager.Default.GetLanguage ((c_type == mirror.DecompileLanguageType.IL ? "msil" : "c-sharp"));
			
			GtkSourceView.SourceBuffer new_buff = new GtkSourceView.SourceBuffer (c_lang);
			new_buff.Text = mirror.Decompilation.decompile_method (c_method, c_type);
			
			sourceView.Buffer = new_buff;
			
			//sourceView.Buffer.Text = mirror.Decompilation.decompile_method( c_method , c_type);
		}
	}


	protected virtual void OnClassViewCursorChanged (object sender, System.EventArgs e)
	{
		
		// I need some way to get the iterator that we've clicked.
		
		TreeViewColumn o_col;
		TreePath o_path;
		
		class_view.GetCursor (out o_path, out o_col);
		
		mangle_path (o_path);
		
	}

	#region getPixbuf and Cache

	Dictionary<string, Gdk.Pixbuf> PixbufCache = new Dictionary<string, Gdk.Pixbuf> ();

	protected Gdk.Pixbuf get_pixbuf (string StockId)
	{
		
		if (PixbufCache.ContainsKey (StockId))
			return PixbufCache[StockId];
		
		Gdk.Pixbuf return_val = null;
		
		try {
			return_val = Gdk.Pixbuf.LoadFromResource ("mirror.icons." + StockId);
		} catch {
			
			// This was "autogenerated" by Stetic when it cranked away at the local icons. 
			// EVIL! This is EVIL!
			//return_val = global::Stetic.IconLoader.LoadIcon(this ,StockId,IconSize.Menu);
			// Instead, we'll use the main window to generate an icon for us. 
			try {
				
				int sz, sy;
				Gtk.Icon.SizeLookup (IconSize.Menu, out sz, out sy);
				return_val = Gtk.IconTheme.Default.LoadIcon (StockId, sz, 0);
				
			// sometimes, this fails. In this case, we should get the normal "missing" image. 
			} catch {
				return_val = this.RenderIcon ("gtk-missing-image", IconSize.Menu, "");
			}
			// if we failed at this point, tough shit.
			
			
			
		} finally {
			// this is to speed up multiple lookups for the same icon. Its a handy trick, saves memory and does some fun stuff for us.
			PixbufCache.Add (StockId, return_val);
		}
		return return_val;
		
		
	}

	#endregion
	protected virtual void OnLangSelectorChanged (object sender, System.EventArgs e)
	{
		decompile_current ();
	}

	protected virtual void OpenLooseAssembly (object sender, System.EventArgs e)
	{
		
		FileChooserDialog diag = new FileChooserDialog ("Open Assembly", this, FileChooserAction.Open, Stock.Open, ResponseType.Ok);
		
		diag.Run ();
		
		string fname = diag.Filename;
		
		diag.Destroy ();
		
		CrankSingleAssembly (fname, true);
	}
	
	
	
}

